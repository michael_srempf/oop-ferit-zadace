#include "klase_i_funkcije.h"
using namespace std;
//konstruktori i funkcije za klasu episode
Episode::Episode() : viewers(0), SumOfGrades(0), higestRating(0), descript() {}
Episode::Episode(int gledatelji, double sumaOc, double najVecaOc, Description opis) : viewers(gledatelji), SumOfGrades(sumaOc), higestRating(najVecaOc) { descript = opis; }

ostream& operator<<(ostream& izlaz, Episode& a) {
	izlaz << a.viewers << ", " << a.SumOfGrades << ", " << a.higestRating << ", " << a.descript << endl;
	return izlaz;
}
istream& operator>>(istream& ulaz, Episode& a) {
	char c[3];
	ulaz >> a.viewers >> c[0] >> a.SumOfGrades >> c[1] >> a.higestRating >> c[2] >> a.descript;
	return ulaz;
}

void Episode::addView(double ocjena) {
	viewers++;
	SumOfGrades += ocjena;
	if (higestRating < ocjena)
	{
		higestRating = ocjena;
	}

}
double Episode::getMaxScore() {
	return higestRating;
}
double Episode::getAverageScore() {
	return SumOfGrades / viewers;
}
int Episode::getViewerCount() {
	return viewers;
}

bool operator>(Episode& a, Episode& b) {
	string x, y;
	x = a.descript.GetName();
	y= b.descript.GetName();
	if (x[1] > 96)x[1] -= 32;
	if (y[1] > 96)y[1] -= 32;
	if (x[0] == y[0])
	{
		return x[1] > y[1];
	}
	return x > y;
}

void sort(Episode** ref, int a) {
	Episode temp;
	for (int i = 0;i < a - 1;i++)
	{
		for (int j = i + 1;j < a;j++)
		{
			if (*ref[i] > * ref[j])
			{
				temp = *ref[i];
				*ref[i] = *ref[j];
				*ref[j] = temp;
			}
		}
	}
}

//konstruktori i funkcije za klasu description
Description::Description() : numberOfEpisode(0), duration(0), nameOfEpisode("NULL") {}
Description::Description(int tnumber, int tduration, string tname) : numberOfEpisode(tnumber), duration(tduration), nameOfEpisode(tname) {}


istream& operator>>(istream& ulaz, Description& a) {
	char c[2];
	ulaz >> a.numberOfEpisode >> c[0] >> a.duration >> c[1];
	getline(ulaz,a.nameOfEpisode);
	return ulaz;
}
ostream& operator<<(ostream& izlaz, Description& a) {
	izlaz << a.numberOfEpisode << ", " << a.duration << ", " << a.nameOfEpisode << endl;
	return izlaz;
}

string Description::GetName() { return nameOfEpisode; }
void print(Episode** ref, int a) {
	for (int i = 0;i < a;i++)
	{
		cout << *ref[i];
	}
}
//globalne funkcije
double generateRandomScore() {
	double f = (double)rand() / RAND_MAX;
	return 10 * f;
}

void persistToFile(string name, Episode** ref, int count) {
	ofstream output(name);
	for (int i = 0;i < count;i++)
	{
		output << *ref[i] << endl;
	}
}
