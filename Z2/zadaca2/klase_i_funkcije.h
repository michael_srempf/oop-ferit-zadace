#pragma once
#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>  
#include <fstream>
#include <stdlib.h>   
#include <stdio.h>
using namespace std;
class Description {
	friend ostream& operator<<(ostream&, Description&);
	friend istream& operator>>(istream&, Description&);
	int numberOfEpisode, duration;
	string nameOfEpisode;
public:
	Description();
	Description(int tnumber, int tduration, string tname);
	string GetName();
};

class Episode {
	friend bool operator>(Episode&, Episode&);
	friend ostream& operator<<(ostream&, Episode&);
	friend istream& operator>>(istream&, Episode&);
	int viewers;
	double SumOfGrades, higestRating;
	Description descript;
public:
	Episode();
	Episode(int gledatelji, double sumaOc, double najVecaOc, Description opis);
	void addView(double ocjena);
	double getMaxScore();
	double getAverageScore();
	int getViewerCount();
	
};
double generateRandomScore();
void persistToFile(string name, Episode**, int);
void print(Episode** ref, int a);
void sort(Episode** ref, int a);