#include <iostream>
#include <stdlib.h>   
#include <stdio.h>
#include <time.h>  

class Episode {
	int viewers;
	double sumaOcijena, najVecaOcjena;
public:
	Episode() :viewers(0), sumaOcijena(0), najVecaOcjena(0) {}
	Episode(int gledatelji, double sumaOc, double najVecaOc) : viewers(gledatelji), sumaOcijena(sumaOc), najVecaOcjena(najVecaOc) {}
	void addView(double ocjena) {
		viewers++;
		sumaOcijena += ocjena;
		if (najVecaOcjena < ocjena)
		{
			najVecaOcjena = ocjena;
		}
		
	}
	double getMaxScore() {
		return najVecaOcjena;
	}
	double getAverageScore() {
		return sumaOcijena/viewers;
	}
	int getViewerCount() {
		return viewers;
	}

};
double generateRandomScore() {
	double f = (double)rand() / RAND_MAX;
	return 10*f;
}

int main()
{
	srand((double)time(NULL));
	Episode* ep1, * ep2;
	ep1 = new Episode();
	ep2 = new Episode(10, 64.39, 8.7);
	int viewers = 10;
	for (int i = 0; i < viewers; i++) {
		ep1->addView(generateRandomScore());
		std::cout << ep1->getMaxScore() << std::endl;
	}
	if (ep1->getAverageScore() > ep2->getAverageScore()) {
		std::cout << "Viewers: " << ep1->getViewerCount() << std::endl;
	}
	else {
		std::cout << "Viewers: " << ep2->getViewerCount() << std::endl;
	}
	delete ep1;
	delete ep2;
	return 0;
}

